package br.ufpr.ees.controlepedidos.android.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import br.ufpr.ees.controlepedidos.android.ControlePedidosApp;
import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.android.service.PedidoService;
import br.ufpr.ees.controlepedidos.android.service.ProdutoService;
import br.ufpr.ees.controlepedidos.android.util.Constants;
import br.ufpr.ees.controlepedidos.android.viewmodel.ProdutoViewModel;
import br.ufpr.ees.controlepedidos.domain.Pedido;
import br.ufpr.ees.controlepedidos.domain.Produto;
import br.ufpr.ees.controlepedidos.android.databinding.ActivityManterProdutoBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManterProdutoActivity extends AppCompatActivity {
    private ProdutoService produtoService;
    private PedidoService pedidoService;
    private ProdutoViewModel produtoViewModel;
    private ActivityManterProdutoBinding produtoBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manter_cliente);

        produtoService = ControlePedidosApp.getProdutoService();
        pedidoService = ControlePedidosApp.getPedidoService();
        produtoViewModel = new ProdutoViewModel();
        produtoBinding = DataBindingUtil.setContentView(this, R.layout.activity_manter_produto);
        produtoBinding.setProduto(produtoViewModel);
        produtoBinding.setManterProdutoActivity(this);

        Intent intent = this.getIntent();
        if (intent.hasExtra("produto")) {
            Produto produto = (Produto) intent.getExtras().get("produto");
            produtoViewModel.setProduto(produto);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (produtoViewModel.getId() == null) {
            produtoBinding.btnExcluir.setVisibility(View.GONE);
        }
    }

    public void salvar(View view) {
        if (isValidForm()) {
            if (produtoViewModel.getId() == null) {
                produtoService.findByDescricao(produtoViewModel.getDescricao()).enqueue(new Callback<Produto>() {
                    @Override
                    public void onResponse(Call<Produto> call, Response<Produto> response) {
                        if (!response.isSuccessful()) {
                            // produto não encontrado (response code = 404 - NOT_FOUND), inserir novo registro
                            produtoService.create(produtoViewModel.getProduto()).enqueue(new Callback<Produto>() {
                                @Override
                                public void onResponse(Call<Produto> call, Response<Produto> response) {
                                    if (response.isSuccessful()) {
                                        Toast.makeText(ManterProdutoActivity.this, Constants.PRODUTO_CREATED_MESSAGE + ": " + response.body().getDescricao(), Toast.LENGTH_LONG).show();
                                        startActivity(new Intent(ManterProdutoActivity.this, ProdutosActivity.class));
                                    }
                                }

                                @Override
                                public void onFailure(Call<Produto> call, Throwable t) {
                                    Log.i("Enqueue Failure", t.getMessage());
                                }
                            });
                        } else {
                            // produto encontrado (response code = 200 - OK), emitir mensagem de erro
                            Toast.makeText(ManterProdutoActivity.this, Constants.PRODUTO_EXISTS_MESSAGE + ": " + produtoViewModel.getDescricao(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Produto> call, Throwable t) {
                        // erro na inserção do produto
                        Toast.makeText(ManterProdutoActivity.this, Constants.PRODUTO_NOT_CREATED_MESSAGE, Toast.LENGTH_LONG).show();
                    }
                });

            } else {
                produtoService.update(produtoViewModel.getProduto(), produtoViewModel.getId()).enqueue(new Callback<Produto>() {
                    @Override
                    public void onResponse(Call<Produto> call, Response<Produto> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(ManterProdutoActivity.this, Constants.PRODUTO_UPDATED_MESSAGE + ": " + response.body().getDescricao(), Toast.LENGTH_LONG).show();
                            startActivity(new Intent(ManterProdutoActivity.this, ProdutosActivity.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<Produto> call, Throwable t) {
                        Log.i("Enqueue Failure", t.getMessage());
                    }
                });
            }
        }
    }

    public void excluir(View view) {
        pedidoService.findByProdutoId(produtoViewModel.getId()).enqueue(new Callback<List<Pedido>>() {
            @Override
            public void onResponse(Call<List<Pedido>> call, Response<List<Pedido>> response) {
                if (!response.isSuccessful()) {
                    produtoService.delete(produtoViewModel.getId()).enqueue(new Callback<Produto>() {
                        @Override
                        public void onResponse(Call<Produto> call, Response<Produto> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(ManterProdutoActivity.this, Constants.PRODUTO_DELETED_MESSAGE + ": " + response.body().getDescricao(), Toast.LENGTH_LONG).show();
                                startActivity(new Intent(ManterProdutoActivity.this, ProdutosActivity.class));
                            }
                        }

                        @Override
                        public void onFailure(Call<Produto> call, Throwable t) {
                            Log.i("Enqueue Failure", t.getMessage());
                        }
                    });
                }
                else {
                    Toast.makeText(ManterProdutoActivity.this, Constants.PRODUTO_BOUNDED_TO_PEDIDO_ERROR, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Pedido>> call, Throwable t) {
                Log.i("Enqueue Failure", t.getMessage());
            }
        });
    }

    private boolean isValidForm() {
        if (produtoBinding.etDescricao.getText().toString().isEmpty()) {
            produtoBinding.txtlayoutDescricao.setErrorEnabled(true);
            produtoBinding.txtlayoutDescricao.setError("Informe a descrição do produto");
            return false;
        } else {
            produtoBinding.txtlayoutDescricao.setErrorEnabled(false);
        }

        return true;
    }
}
