package br.ufpr.ees.controlepedidos.android.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;

import br.ufpr.ees.controlepedidos.domain.Produto;

public class ProdutoViewModel implements Observable {

    private PropertyChangeRegistry propertyChangeRegistry = new PropertyChangeRegistry();

    private Produto produto;

    public ProdutoViewModel() {
        produto = new Produto();
    }

    public ProdutoViewModel(Produto produto) {
        this.produto = produto;
    }

    @Bindable
    public String getDescricao() {
        return produto.getDescricao();
    }

    public void setDescricao(String cpf) {
        produto.setDescricao(cpf);
    }

    @Bindable
    public Long getId() {
        return produto.getId();
    }

    public void setId(Long id) {
        produto.setId(id);
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        propertyChangeRegistry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        propertyChangeRegistry.remove(callback);
    }
}
