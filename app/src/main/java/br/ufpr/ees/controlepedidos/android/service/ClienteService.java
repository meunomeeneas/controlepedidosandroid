package br.ufpr.ees.controlepedidos.android.service;

import java.util.List;

import br.ufpr.ees.controlepedidos.domain.Cliente;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ClienteService {

    @GET("/clientes/")
    Call<List<Cliente>> findAll();

    @GET("/clientes/{id}")
    Call<Cliente> findById(@Path("id") Long id);

    @GET("/clientes/cpf/{cpf}")
    Call<Cliente> findByCpf(@Path("cpf") String cpf);

    @POST("/clientes/")
    Call<Cliente> create(@Body Cliente cliente);

    @PUT("/clientes/{id}")
    Call<Cliente> update(@Body Cliente cliente, @Path("id") Long id);

    @DELETE("/clientes/{id}")
    Call<Cliente> delete(@Path("id") Long id);
}
