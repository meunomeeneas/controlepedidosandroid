package br.ufpr.ees.controlepedidos.android.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.android.databinding.LayoutItensPedidoBinding;
import br.ufpr.ees.controlepedidos.domain.ItemPedido;

public class ItensPedidoAdapter extends BaseAdapter {
    private List<ItemPedido> itensPedido;
    private final Activity activity;

    public ItensPedidoAdapter(List<ItemPedido> itensPedido, Activity activity) {
        this.itensPedido = itensPedido;
        this.activity = activity;
    }

    public List<ItemPedido> getItensPedido() {
        return itensPedido;
    }

    @Override
    public int getCount() {
        return itensPedido.size();
    }

    @Override
    public Object getItem(int position) {
        return itensPedido.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public boolean removerItemPedido(int posicao) {
        this.itensPedido.remove(posicao);
        notifyDataSetChanged();
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutItensPedidoBinding itensPedidoBinding;

        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.layout_itens_pedido, null);
            itensPedidoBinding = DataBindingUtil.bind(convertView);
            convertView.setTag(itensPedidoBinding);
        } else {
            itensPedidoBinding = (LayoutItensPedidoBinding) convertView.getTag();
        }
        itensPedidoBinding.setItemPedido(this.itensPedido.get(position));
        return itensPedidoBinding.getRoot();
    }

    public void incluirProduto(ItemPedido itemPedido) {
        // percorrer a ListView de itens de pedido em memória para verificar se o item já foi incluido
        // se o item já estiver na lista, somar com a nova quantidade informada
//        boolean itemEncontrado = false;
//        for (ItemPedido item: itensPedido) {
//            if (item.getProduto().getId() == itemPedido.getProduto().getId()) {
//                itemEncontrado = true;
//                item.setQuantidade(item.getQuantidade() + itemPedido.getQuantidade());
//                break;
//            }
//        }
//
//        if (!itemEncontrado) {
//            this.itensPedido.add(itemPedido);
//        }
        int indexItem = itensPedido.indexOf(itemPedido);
        if (indexItem != -1) {
            itensPedido.get(indexItem).setQuantidade(itensPedido.get(indexItem).getQuantidade() + itemPedido.getQuantidade());
        } else {
            this.itensPedido.add(itemPedido);
        }

        this.notifyDataSetChanged();
    }
}
