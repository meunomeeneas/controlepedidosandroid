package br.ufpr.ees.controlepedidos.android.util;

public class Constants {
    public static final String API_URL = "https://controlepedidosbackend.herokuapp.com";
    public static final String CLIENT_CREATED_MESSAGE = "Cliente inserido";
    public static final String CLIENTE_DELETED_MESSAGE = "Cliente excluído";
    public static final String CLIENTE_NOT_DELETED_ERROR = "Erro na exclusão do cliente";
    public static final String CLIENTE_BOUNDED_TO_PEDIDO_ERROR = "Este cliente está vinculado a pedido(s) e não pode ser excluído";
    public static final String CLIENT_NOT_CREATED_MESSAGE = "Erro na inserção do cliente";
    public static final String CLIENT_NOT_FOUND_MESSAGE = "Cliente não encontrado";
    public static final String CLIENT_UPDATED_MESSAGE = "Cliente alterado";
    public static final String ORDER_CREATED_MESSAGE = "Pedido inserido";
    public static final String ORDER_NOT_CREATED_MESSAGE = "Erro na inserção do pedido";
    public static final String INVALID_CPF = "CPF inválido";
    public static final String ITENS_NOT_SELECTED = "Selecione ao menos um item para o pedido";
    public static final String CLIENT_EXISTS_MESSAGE = "CPF já cadastrado";
    public static final String PRODUTO_CREATED_MESSAGE = "Produto inserido";
    public static final String PRODUTO_EXISTS_MESSAGE = "Produto já cadastrado";
    public static final String PRODUTO_NOT_CREATED_MESSAGE = "Erro na inserção do produto";
    public static final String PRODUTO_UPDATED_MESSAGE = "Produto alterado";
    public static final String PRODUTO_DELETED_MESSAGE = "Produto excluído";
    public static final String PRODUTO_BOUNDED_TO_PEDIDO_ERROR = "Este produto está vinculado a pedido(s) e não pode ser excluído";
    public static final String REQUEST_ERROR = "Erro na execução da requisição";
    public static final String PEDIDO_NOT_EXISTS = "Não existem pedidos para esse cliente.";
}
