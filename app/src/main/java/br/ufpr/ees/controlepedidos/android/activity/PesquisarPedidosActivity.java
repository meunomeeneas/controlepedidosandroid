package br.ufpr.ees.controlepedidos.android.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import br.ufpr.ees.controlepedidos.android.ControlePedidosApp;
import br.ufpr.ees.controlepedidos.android.adapter.PedidoAdapter;
import br.ufpr.ees.controlepedidos.android.databinding.ActivityPesquisarPedidosBinding;
import br.ufpr.ees.controlepedidos.android.service.ClienteService;
import br.ufpr.ees.controlepedidos.android.service.PedidoService;
import br.ufpr.ees.controlepedidos.android.util.CPFUtil;
import br.ufpr.ees.controlepedidos.android.util.Constants;
import br.ufpr.ees.controlepedidos.android.util.MaskEditUtil;
import br.ufpr.ees.controlepedidos.android.viewmodel.ClienteViewModel;

import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.domain.Cliente;
import br.ufpr.ees.controlepedidos.domain.Pedido;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static br.ufpr.ees.controlepedidos.android.util.MaskEditUtil.unmask;

public class PesquisarPedidosActivity extends AppCompatActivity {

    private ActivityPesquisarPedidosBinding pesquisarPedidosActivityBinding;
    private ClienteViewModel clienteViewModel;
    private ClienteService clienteService;
    private PedidoService pedidoService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        clienteService = ControlePedidosApp.getClienteService();

        pesquisarPedidosActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_pesquisar_pedidos);

        clienteViewModel = new ClienteViewModel();
        pesquisarPedidosActivityBinding.setCliente(clienteViewModel);

        pesquisarPedidosActivityBinding.etCpf.addTextChangedListener(MaskEditUtil.mask(
                pesquisarPedidosActivityBinding.etCpf, MaskEditUtil.FORMAT_CPF));

    }

    public void pesquisar(View view) {
        if (isValidForm()) {
            final String cpfFormatado = clienteViewModel.getCpf();
            clienteViewModel.setCpf(unmask(clienteViewModel.getCpf()));
            clienteService.findByCpf(clienteViewModel.getCpf()).enqueue(new Callback<Cliente>() {
                @Override
                public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                    if (!response.isSuccessful()) {
                        // CPF não encontrado, emitir mensagem de erro
                        Toast.makeText(PesquisarPedidosActivity.this, Constants.CLIENT_NOT_FOUND_MESSAGE + ": " + cpfFormatado, Toast.LENGTH_LONG).show();
                    } else {
                        // CPF encontrado, chamar tela de listagem de pedidos
                        clienteViewModel.setCliente(response.body());
                        pedidoService = ControlePedidosApp.getPedidoService();
                        pedidoService.findByClienteCpf(clienteViewModel.getCpf()).enqueue(new Callback<List<Pedido>>() {
                            @Override
                            public void onResponse(Call<List<Pedido>> call, Response<List<Pedido>> response) {
                                if (!response.isSuccessful()){
                                    Toast.makeText(PesquisarPedidosActivity.this, Constants.PEDIDO_NOT_EXISTS, Toast.LENGTH_LONG).show();
                                }
                                else {
                                    Intent listarPedidosIntent = new Intent(PesquisarPedidosActivity.this, ListarPedidosActivity.class);
                                    listarPedidosIntent.putExtra("cliente", clienteViewModel.getCliente());
                                    startActivity(listarPedidosIntent);
                                }
                            }
                            @Override
                            public void onFailure(Call<List<Pedido>> call, Throwable t) {
                                Log.i("Enqueue Failure", t.getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<Cliente> call, Throwable t) {
                    // erro na pesquisa do cliente
                    Toast.makeText(PesquisarPedidosActivity.this, Constants.REQUEST_ERROR + ": " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private boolean isValidForm() {
        if (pesquisarPedidosActivityBinding.etCpf.getText().toString().isEmpty()) {
            pesquisarPedidosActivityBinding.txtlayoutCpf.setErrorEnabled(true);
            pesquisarPedidosActivityBinding.txtlayoutCpf.setError("Informe o CPF do cliente");
            return false;
        } else {
            pesquisarPedidosActivityBinding.txtlayoutCpf.setErrorEnabled(false);
        }

        if (!CPFUtil.isValid(pesquisarPedidosActivityBinding.etCpf.getText().toString())) {
            pesquisarPedidosActivityBinding.txtlayoutCpf.setErrorEnabled(true);
            pesquisarPedidosActivityBinding.txtlayoutCpf.setError("CPF inválido");
            return false;
        } else {
            pesquisarPedidosActivityBinding.txtlayoutCpf.setErrorEnabled(false);
        }
        return true;
    }

}
