package br.ufpr.ees.controlepedidos.android.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import br.ufpr.ees.controlepedidos.android.ControlePedidosApp;
import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.android.databinding.ActivityManterClienteBinding;
import br.ufpr.ees.controlepedidos.android.service.ClienteService;
import br.ufpr.ees.controlepedidos.android.service.PedidoService;
import br.ufpr.ees.controlepedidos.android.util.CPFUtil;
import br.ufpr.ees.controlepedidos.android.util.Constants;
import br.ufpr.ees.controlepedidos.android.util.MaskEditUtil;
import br.ufpr.ees.controlepedidos.android.viewmodel.ClienteViewModel;
import br.ufpr.ees.controlepedidos.domain.Cliente;
import br.ufpr.ees.controlepedidos.domain.Pedido;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static br.ufpr.ees.controlepedidos.android.util.MaskEditUtil.unmask;

public class ManterClienteActivity extends AppCompatActivity {
    private ClienteService clienteService;
    private PedidoService pedidoService;
    private ClienteViewModel clienteViewModel;
    private ActivityManterClienteBinding clienteBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manter_cliente);

        clienteService = ControlePedidosApp.getClienteService();
        pedidoService = ControlePedidosApp.getPedidoService();

        clienteViewModel = new ClienteViewModel();
        clienteBinding = DataBindingUtil.setContentView(this, R.layout.activity_manter_cliente);
        clienteBinding.setCliente(clienteViewModel);
        clienteBinding.setManterClienteActivity(this);

        Intent intent = this.getIntent();
        if (intent.hasExtra("cliente")) {
            Cliente cliente = (Cliente) intent.getExtras().get("cliente");
            clienteViewModel.setCliente(cliente);
            clienteBinding.etCpf.setEnabled(false);
        }

        clienteBinding.etCpf.addTextChangedListener(MaskEditUtil.mask(clienteBinding.etCpf, MaskEditUtil.FORMAT_CPF));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (clienteViewModel.getId() == null) {
            clienteBinding.btnExcluir.setVisibility(View.GONE);
        }

    }

    public void salvar(View view) {
        if (isValidForm()) {
            if (clienteViewModel.getId() == null) {
                final String cpfFormatado = clienteViewModel.getCpf();
                clienteViewModel.setCpf(unmask(clienteViewModel.getCpf()));

                // testa se o CPF informado já está cadastrado
                clienteService.findByCpf(clienteViewModel.getCpf()).enqueue(new Callback<Cliente>() {
                    @Override
                    public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                        if (!response.isSuccessful()) {
                            // CPF não encontrado, inserir novo registro
                            clienteService.create(clienteViewModel.getCliente()).enqueue(new Callback<Cliente>() {
                                @Override
                                public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                                    if (response.isSuccessful()) {
                                        Toast.makeText(ManterClienteActivity.this, Constants.CLIENT_CREATED_MESSAGE + ": " + response.body().getNome(), Toast.LENGTH_LONG).show();
                                        startActivity(new Intent(ManterClienteActivity.this, ClientesActivity.class));
                                    }
                                }

                                @Override
                                public void onFailure(Call<Cliente> call, Throwable t) {
                                    Log.i("Enqueue Failure", t.getMessage());
                                }
                            });
                        } else {
                            // CPF encontrado, emitir mensagem de erro
                            Toast.makeText(ManterClienteActivity.this, Constants.CLIENT_EXISTS_MESSAGE + ": " + cpfFormatado, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Cliente> call, Throwable t) {
                        // erro na inserção do cliente
                        Toast.makeText(ManterClienteActivity.this, Constants.CLIENT_NOT_CREATED_MESSAGE, Toast.LENGTH_LONG).show();
                    }
                });

            } else {
                clienteViewModel.setCpf(unmask(clienteViewModel.getCpf()));
                clienteService.update(clienteViewModel.getCliente(), clienteViewModel.getId()).enqueue(new Callback<Cliente>() {
                    @Override
                    public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(ManterClienteActivity.this, Constants.CLIENT_UPDATED_MESSAGE + ": " + response.body().getNome(), Toast.LENGTH_LONG).show();
                            startActivity(new Intent(ManterClienteActivity.this, ClientesActivity.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<Cliente> call, Throwable t) {
                        Log.i("Enqueue Failure", t.getMessage());
                    }
                });
            }
        }
    }

    public void excluir(View view) {
        pedidoService.findByClienteCpf(unmask(clienteViewModel.getCpf())).enqueue(new Callback<List<Pedido>>() {
            @Override
            public void onResponse(Call<List<Pedido>> call, Response<List<Pedido>> response) {
                if (!response.isSuccessful()) {
                    // pedidos não encontrados (response code = 404 - NOT_FOUND), prosseguir com a exclusão do cliente
                    clienteService.delete(clienteViewModel.getId()).enqueue(new Callback<Cliente>() {
                        @Override
                        public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(ManterClienteActivity.this, Constants.CLIENTE_DELETED_MESSAGE + ": " + MaskEditUtil.formatCPF(response.body().getCpf()), Toast.LENGTH_LONG).show();
                                startActivity(new Intent(ManterClienteActivity.this, ClientesActivity.class));
                            }
                        }

                        @Override
                        public void onFailure(Call<Cliente> call, Throwable t) {
                            Toast.makeText(ManterClienteActivity.this, Constants.CLIENTE_NOT_DELETED_ERROR + ": " + t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
                else {
                    // pedidos encontrados (response code = 200 - OK), emitir mensagem de erro
                    Toast.makeText(ManterClienteActivity.this, Constants.CLIENTE_BOUNDED_TO_PEDIDO_ERROR, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Pedido>> call, Throwable t) {
                Log.i("Enqueue Failure", t.getMessage());
            }
        });
    }

    private boolean isValidForm() {
        if (clienteBinding.etNome.getText().toString().isEmpty()) {
            clienteBinding.txtlayoutNome.setErrorEnabled(true);
            clienteBinding.txtlayoutNome.setError("Informe o nome do cliente");
            return false;
        } else {
            clienteBinding.txtlayoutNome.setErrorEnabled(false);
        }

        if (clienteBinding.etSobrenome.getText().toString().isEmpty()) {
            clienteBinding.txtlayoutSobrenome.setErrorEnabled(true);
            clienteBinding.txtlayoutSobrenome.setError("Informe o sobrenome do cliente");
            return false;
        } else {
            clienteBinding.txtlayoutSobrenome.setErrorEnabled(false);
        }

        if (clienteBinding.etCpf.getText().toString().isEmpty()) {
            clienteBinding.txtlayoutCpf.setErrorEnabled(true);
            clienteBinding.txtlayoutCpf.setError("Informe o CPF do cliente");
            return false;
        } else {
            clienteBinding.txtlayoutCpf.setErrorEnabled(false);
        }

        if (!CPFUtil.isValid(clienteBinding.etCpf.getText().toString())) {
            clienteBinding.txtlayoutCpf.setErrorEnabled(true);
            clienteBinding.txtlayoutCpf.setError("CPF inválido");
            return false;
        } else {
            clienteBinding.txtlayoutCpf.setErrorEnabled(false);
        }
        return true;
    }
}
