package br.ufpr.ees.controlepedidos.android.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.List;

import br.ufpr.ees.controlepedidos.android.ControlePedidosApp;
import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.android.adapter.ProdutoAdapter;
import br.ufpr.ees.controlepedidos.android.service.ProdutoService;
import br.ufpr.ees.controlepedidos.domain.Produto;
import br.ufpr.ees.controlepedidos.android.databinding.ActivityProdutosBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProdutosActivity extends AppCompatActivity {

    private ProdutoService produtoService;
    private List<Produto> produtos;
    private ProdutoAdapter adapter;
    private ActivityProdutosBinding produtoBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        produtoBinding = DataBindingUtil.setContentView(this, R.layout.activity_produtos);

        produtoBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ManterProdutoActivity.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        produtoService = ControlePedidosApp.getProdutoService();
        produtos = new ArrayList<>();

        produtoService.findAll().enqueue(new Callback<List<Produto>>() {
            @Override
            public void onResponse(Call<List<Produto>> call, Response<List<Produto>> response) {
                produtos = response.body();
                adapter = new ProdutoAdapter(produtos, ProdutosActivity.this);
                produtoBinding.listaProdutos.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Produto>> call, Throwable t) {
                Log.i("Enqueue Failure", t.getMessage());
            }
        });

        adapter = new ProdutoAdapter(produtos, ProdutosActivity.this);
        produtoBinding.listaProdutos.setAdapter(adapter);

        produtoBinding.listaProdutos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent manterProdutoIntent = new Intent(ProdutosActivity.this, ManterProdutoActivity.class);
                Produto produto =  produtos.get(position);
                manterProdutoIntent.putExtra("produto", produto);
                startActivity(manterProdutoIntent);
            }
        });

    }
}
