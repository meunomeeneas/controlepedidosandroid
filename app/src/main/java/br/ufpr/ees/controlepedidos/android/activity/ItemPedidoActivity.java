package br.ufpr.ees.controlepedidos.android.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.android.adapter.ItensPedidoAdapter;
import br.ufpr.ees.controlepedidos.android.databinding.ActivityItemPedidoBinding;
import br.ufpr.ees.controlepedidos.android.util.MaskEditUtil;
import br.ufpr.ees.controlepedidos.android.viewmodel.PedidoViewModel;
import br.ufpr.ees.controlepedidos.domain.Pedido;

public class ItemPedidoActivity extends AppCompatActivity {

    private ActivityItemPedidoBinding itemPedidoBinding;
    private PedidoViewModel pedidoViewModel;
    private ItensPedidoAdapter itensPedidoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        itemPedidoBinding = DataBindingUtil.setContentView(this, R.layout.activity_item_pedido);
        pedidoViewModel = new PedidoViewModel();

        Intent intent = this.getIntent();
        if (intent.hasExtra("pedido")) {
            Pedido pedido = (Pedido) intent.getExtras().get("pedido");
            pedidoViewModel.setPedido(pedido);
        }

        itemPedidoBinding.setPedido(pedidoViewModel);
        itemPedidoBinding.setCpf(MaskEditUtil.formatCPF(pedidoViewModel.getCliente().getCpf()));
    }

    @Override
    protected void onStart() {
        super.onStart();

        // seta o adapter à listview de itens de pedido
        this.itensPedidoAdapter = new ItensPedidoAdapter(pedidoViewModel.getPedido().getItensPedido(), ItemPedidoActivity.this);
        itemPedidoBinding.listaItensPedido.setAdapter(this.itensPedidoAdapter);

    }
}
