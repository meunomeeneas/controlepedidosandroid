package br.ufpr.ees.controlepedidos.android.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.databinding.PropertyChangeRegistry;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ufpr.ees.controlepedidos.domain.Cliente;
import br.ufpr.ees.controlepedidos.domain.ItemPedido;
import br.ufpr.ees.controlepedidos.domain.Pedido;

public class PedidoViewModel implements Observable {

    private PropertyChangeRegistry propertyChangeRegistry = new PropertyChangeRegistry();

    public final ObservableField<String> data = new ObservableField<>();

    private Pedido pedido;

    public PedidoViewModel() {

        pedido = new Pedido();
        pedido.setItensPedido(new ArrayList<ItemPedido>());
    }

    public void setDataPedido(Date dataPedido) { pedido.setDataPedido(dataPedido); }

    @Bindable
    public Cliente getCliente() {
        return pedido.getCliente();
    }

    public void setCliente(Cliente cliente) { pedido.setCliente(cliente); }

    public void setItensPedido(List<ItemPedido> itensPedido) {
        pedido.setItensPedido(itensPedido);
    }

    @Bindable
    public Long getId() {
        return pedido.getId();
    }

    public void setId(Long id) {
        pedido.setId(id);
    }

    public Pedido getPedido() { return pedido; };

    public void setPedido(Pedido pedido) { this.pedido = pedido; }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        propertyChangeRegistry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        propertyChangeRegistry.remove(callback);
    }

    @Bindable
    public String getData() {
        return new SimpleDateFormat("dd/MM/yyyy").format(this.pedido.getDataPedido());
    }
}
