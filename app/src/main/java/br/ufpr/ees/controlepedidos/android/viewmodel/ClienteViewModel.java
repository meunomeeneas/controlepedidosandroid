package br.ufpr.ees.controlepedidos.android.viewmodel;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;

import br.ufpr.ees.controlepedidos.domain.Cliente;

public class ClienteViewModel implements Observable {

    private PropertyChangeRegistry propertyChangeRegistry = new PropertyChangeRegistry();

    private Cliente cliente;

    public ClienteViewModel() {
        cliente = new Cliente();
    }

    public ClienteViewModel(Cliente cliente) {
        this.cliente = cliente;
    }

    @Bindable
    public String getCpf() {
        return cliente.getCpf();
    }

    public void setCpf(String cpf) {
        cliente.setCpf(cpf);
    }

    @Bindable
    public String getNome() {
        return cliente.getNome();
    }

    public void setNome(String nome) {
        cliente.setNome(nome);
    }

    @Bindable
    public String getSobrenome() {
        return cliente.getSobrenome();
    }

    public void setSobrenome(String sobrenome) {
        cliente.setSobrenome(sobrenome);
    }

    @Bindable
    public Long getId() {
        return cliente.getId();
    }

    public void setId(Long id) {
        cliente.setId(id);
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        propertyChangeRegistry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        propertyChangeRegistry.remove(callback);
    }

}
