package br.ufpr.ees.controlepedidos.android.service;

import java.util.List;

import br.ufpr.ees.controlepedidos.domain.Produto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ProdutoService {

    @GET("/produtos/")
    Call<List<Produto>> findAll();

    @GET("/produtos/{id}")
    Call<Produto> findById(@Path("id") Long id);

    @GET("/produtos/find/")
    Call<Produto> findByDescricao(@Query("descricao") String descricao);

    @POST("/produtos/")
    Call<Produto> create(@Body Produto cliente);

    @PUT("/produtos/{id}")
    Call<Produto> update(@Body Produto cliente, @Path("id") Long id);

    @DELETE("/produtos/{id}")
    Call<Produto> delete(@Path("id") Long id);
}
