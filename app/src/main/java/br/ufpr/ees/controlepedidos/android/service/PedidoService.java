package br.ufpr.ees.controlepedidos.android.service;

import java.util.List;

import br.ufpr.ees.controlepedidos.domain.Pedido;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PedidoService {

    @GET("/pedidos/")
    Call<List<Pedido>> findAll();

    @GET("/pedidos/{id}")
    Call<Pedido> findById(@Path("id") Long id);

    @GET("/pedidos/cliente/{cpf}")
    Call<List<Pedido>> findByClienteCpf(@Path("cpf") String cpf);

    @GET("/pedidos/produto/{id}")
    Call<List<Pedido>> findByProdutoId(@Path("id") Long id);

    @POST("/pedidos/")
    Call<Pedido> create(@Body Pedido pedido);

    @PUT("/pedidos/{id}")
    Call<Pedido> update(@Body Pedido pedido, @Path("id") Long id);

    @DELETE("/pedidos/{id}")
    Call<Pedido> delete(@Path("id") Long id);
}
