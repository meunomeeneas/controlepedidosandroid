package br.ufpr.ees.controlepedidos.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.ufpr.ees.controlepedidos.android.ControlePedidosApp;
import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.android.adapter.ClienteAdapter;
import br.ufpr.ees.controlepedidos.android.service.ClienteService;
import br.ufpr.ees.controlepedidos.android.util.MaskEditUtil;
import br.ufpr.ees.controlepedidos.domain.Cliente;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientesActivity extends AppCompatActivity {

    private ClienteService clienteService;
    private List<Cliente> clientes;
    private ClienteAdapter adapter;
    private ListView v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ManterClienteActivity.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        clienteService = ControlePedidosApp.getClienteService();
        clientes = new ArrayList<>();
        v = findViewById(R.id.lista_cliente);

        clienteService.findAll().enqueue(new Callback<List<Cliente>>() {
            @Override
            public void onResponse(Call<List<Cliente>> call, Response<List<Cliente>> response) {
                clientes = response.body();
                // tratar campo CPF: preencher com zeros à esquerda se necessário e aplicar máscara
                for (Cliente c : clientes) {
                    c.setCpf(MaskEditUtil.formatCPF(c.getCpf()));
                }
                adapter = new ClienteAdapter(clientes, ClientesActivity.this);
                v.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Cliente>> call, Throwable t) {
                Log.i("Enqueue Failure", t.getMessage());
            }
        });

        adapter = new ClienteAdapter(clientes, ClientesActivity.this);
        v.setAdapter(adapter);

        v.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent manterClienteIntent = new Intent(ClientesActivity.this, ManterClienteActivity.class);
                Cliente cliente =  clientes.get(position);
                manterClienteIntent.putExtra("cliente", cliente);
                startActivity(manterClienteIntent);
            }
        });

    }
}
