package br.ufpr.ees.controlepedidos.android.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.ufpr.ees.controlepedidos.android.ControlePedidosApp;
import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.android.adapter.ItensPedidoAdapter;
import br.ufpr.ees.controlepedidos.android.databinding.ActivityPedidosBinding;
import br.ufpr.ees.controlepedidos.android.service.ClienteService;
import br.ufpr.ees.controlepedidos.android.service.PedidoService;
import br.ufpr.ees.controlepedidos.android.service.ProdutoService;
import br.ufpr.ees.controlepedidos.android.util.CPFUtil;
import br.ufpr.ees.controlepedidos.android.util.Constants;
import br.ufpr.ees.controlepedidos.android.util.MaskEditUtil;
import br.ufpr.ees.controlepedidos.android.viewmodel.PedidoViewModel;
import br.ufpr.ees.controlepedidos.domain.Cliente;
import br.ufpr.ees.controlepedidos.domain.ItemPedido;
import br.ufpr.ees.controlepedidos.domain.Pedido;
import br.ufpr.ees.controlepedidos.domain.Produto;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PedidosActivity extends AppCompatActivity {

    private ClienteService clienteService;
    private ProdutoService produtoService;
    private PedidoService pedidoService;
    private PedidoViewModel pedidoViewModel;
    private ActivityPedidosBinding pedidoBinding;

    private ItensPedidoAdapter itensPedidoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pedidoService = ControlePedidosApp.getPedidoService();
        clienteService = ControlePedidosApp.getClienteService();
        produtoService = ControlePedidosApp.getProdutoService();

        pedidoViewModel = new PedidoViewModel();

        pedidoBinding = DataBindingUtil.setContentView(this, R.layout.activity_pedidos);
        pedidoBinding.setPedido(pedidoViewModel);
        pedidoBinding.setPedidosActivity(this);

        pedidoBinding.etCpf.addTextChangedListener(MaskEditUtil.mask(pedidoBinding.etCpf, MaskEditUtil.FORMAT_CPF));
    }

    @Override
    protected void onStart() {
        super.onStart();

        // carrega spinner de produtos
        produtoService.findAll().enqueue(new Callback<List<Produto>>() {
            @Override
            public void onResponse(Call<List<Produto>> call, Response<List<Produto>> response) {
                ArrayAdapter<Produto> spnProdutoAdapter = new ArrayAdapter<Produto>(PedidosActivity.this,
                        android.R.layout.simple_spinner_item,
                        response.body());
                pedidoBinding.spnProduto.setAdapter(spnProdutoAdapter);
            }

            @Override
            public void onFailure(Call<List<Produto>> call, Throwable t) {
                Log.i("Enqueue Failure", t.getMessage());
            }
        });

        // seta o adapter à listview de itens de pedido
        this.itensPedidoAdapter = new ItensPedidoAdapter(new ArrayList<ItemPedido>(), PedidosActivity.this);
        pedidoBinding.listaItensPedido.setAdapter(this.itensPedidoAdapter);

        pedidoBinding.listaItensPedido.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final ItemPedido itemPedido = (ItemPedido) itensPedidoAdapter.getItem(position);

                AlertDialog.Builder janelaEscolha = new AlertDialog.Builder(PedidosActivity.this);
                janelaEscolha.setTitle("Remover item");
                janelaEscolha.setMessage("Deseja remover o item " + itemPedido.getProduto().getDescricao() + "?");

                janelaEscolha.setNegativeButton("Não", null);
                janelaEscolha.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean excluiu = false;

                        excluiu = itensPedidoAdapter.removerItemPedido(position);

                        if (!excluiu) {
                            Toast.makeText(PedidosActivity.this,
                                    "Erro ao excluir item do pedido", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                janelaEscolha.create().show();
            }
        });
    }

    public void incluirProduto(View view) {
        // inclui produto selecionado no spinner à listview de itens de pedido
        ItemPedido itemPedido = new ItemPedido();
        Produto produto = (Produto) pedidoBinding.spnProduto.getSelectedItem();
        Integer quantidade = 0;

        if (pedidoBinding.etQuantidade.getText().toString().equals("")) {
            quantidade = 1;
        } else {
            quantidade = Integer.parseInt(pedidoBinding.etQuantidade.getText().toString());
        }

        itemPedido.setQuantidade(quantidade);
        itemPedido.setProduto(produto);

        this.itensPedidoAdapter.incluirProduto(itemPedido);
        pedidoBinding.etQuantidade.setText("");
    }

    private boolean isValidForm() {
        // testa se o usuário informou um CPF
        if (pedidoBinding.etCpf.getText().toString().isEmpty()) {
            pedidoBinding.txtlayoutCpf.setErrorEnabled(true);
            pedidoBinding.txtlayoutCpf.setError("Informe o CPF do cliente");
            return false;
        } else {
            pedidoBinding.txtlayoutCpf.setErrorEnabled(false);
        }

        // testa se o CPF informado é válido
        if (!CPFUtil.isValid(pedidoBinding.etCpf.getText().toString())) {
            Toast.makeText(PedidosActivity.this, Constants.INVALID_CPF + ": " + pedidoBinding.etCpf.getText().toString(), Toast.LENGTH_LONG).show();
            return false;
        }

        // testa se o usuário escolheu itens para o pedido
        if (this.itensPedidoAdapter.getItensPedido().size() == 0) {
            Toast.makeText(PedidosActivity.this, Constants.ITENS_NOT_SELECTED, Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public void salvar(View view) {
        if (isValidForm()) {
            // campos informados corretamente, processa o pedido
            pedidoViewModel.setDataPedido(Calendar.getInstance().getTime());
            pedidoViewModel.setItensPedido(this.itensPedidoAdapter.getItensPedido());

            // procura o cliente pelo CPF informado
            clienteService.findByCpf(pedidoBinding.etCpf.getText().toString().trim().replace(".", "").replace("-", "")).enqueue(new Callback<Cliente>() {
                @Override
                public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                    if (response.isSuccessful()) {
                        // se encontrou o cliente, grava o pedido e os itens no banco de dados
                        pedidoViewModel.setCliente(response.body());
                        pedidoService.create(pedidoViewModel.getPedido()).enqueue(new Callback<Pedido>() {
                            @Override
                            public void onResponse(Call<Pedido> call, Response<Pedido> response) {
                                if (response.isSuccessful()) {
                                    Toast.makeText(PedidosActivity.this, Constants.ORDER_CREATED_MESSAGE + ": " + response.body().getId(), Toast.LENGTH_LONG).show();
                                }
                                else {
                                    Toast.makeText(PedidosActivity.this, Constants.ORDER_NOT_CREATED_MESSAGE + ": " + response.errorBody(), Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Pedido> call, Throwable t) {
                                Log.i("Enqueue Failure", t.getMessage());
                            }
                        });
                    }
                    else {
                        // cliente não encontrado
                        Toast.makeText(PedidosActivity.this, Constants.CLIENT_NOT_FOUND_MESSAGE + ": " + pedidoBinding.etCpf.getText().toString(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Cliente> call, Throwable t) {
                    Log.i("Enqueue Failure", t.getMessage());
                }
            });
            startActivity(new Intent(this, PedidosActivity.class));
        }
    }
}
