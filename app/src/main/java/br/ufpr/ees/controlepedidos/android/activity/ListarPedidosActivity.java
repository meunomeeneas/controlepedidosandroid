package br.ufpr.ees.controlepedidos.android.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.ufpr.ees.controlepedidos.android.ControlePedidosApp;
import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.android.adapter.PedidoAdapter;
import br.ufpr.ees.controlepedidos.android.databinding.ActivityListarPedidosBinding;
import br.ufpr.ees.controlepedidos.android.service.PedidoService;
import br.ufpr.ees.controlepedidos.android.util.Constants;
import br.ufpr.ees.controlepedidos.android.util.MaskEditUtil;
import br.ufpr.ees.controlepedidos.android.viewmodel.ClienteViewModel;
import br.ufpr.ees.controlepedidos.domain.Cliente;
import br.ufpr.ees.controlepedidos.domain.Pedido;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListarPedidosActivity extends AppCompatActivity {

    private PedidoService pedidoService;
    private ClienteViewModel clienteViewModel;
    private List<Pedido> pedidos;
    private PedidoAdapter pedidoAdapter;
    private ActivityListarPedidosBinding listarPedidosBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listarPedidosBinding = DataBindingUtil.setContentView(this, R.layout.activity_listar_pedidos);
        pedidoService = ControlePedidosApp.getPedidoService();
        clienteViewModel = new ClienteViewModel();

        Intent intent = this.getIntent();
        if (intent.hasExtra("cliente")) {
            Cliente cliente = (Cliente) intent.getExtras().get("cliente");
            clienteViewModel.setCliente(cliente);
        }

        listarPedidosBinding.setCliente(clienteViewModel);
    }

    @Override
    protected void onStart() {

        super.onStart();
        pedidoService = ControlePedidosApp.getPedidoService();
        pedidos = new ArrayList<>();

        pedidoService.findByClienteCpf(clienteViewModel.getCpf()).enqueue(new Callback<List<Pedido>>() {
            @Override
            public void onResponse(Call<List<Pedido>> call, Response<List<Pedido>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(ListarPedidosActivity.this, Constants.PEDIDO_NOT_EXISTS, Toast.LENGTH_LONG).show();
                } else {
                    pedidos = response.body();
                    pedidoAdapter = new PedidoAdapter(pedidos, ListarPedidosActivity.this);
                    listarPedidosBinding.listaPedidos.setAdapter(pedidoAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<Pedido>> call, Throwable t) {
                Log.i("Enqueue Failure", t.getMessage());
            }
        });

        listarPedidosBinding.setCpf(MaskEditUtil.formatCPF(clienteViewModel.getCpf()));

        pedidoAdapter = new PedidoAdapter(pedidos, ListarPedidosActivity.this);
        listarPedidosBinding.listaPedidos.setAdapter(pedidoAdapter);

        listarPedidosBinding.listaPedidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent itemPedidoIntent = new Intent(ListarPedidosActivity.this, ItemPedidoActivity.class);
                Pedido pedido = pedidos.get(position);
                itemPedidoIntent.putExtra("pedido", pedido);
                startActivity(itemPedidoIntent);
            }
        });

    }
}
