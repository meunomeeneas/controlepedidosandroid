package br.ufpr.ees.controlepedidos.android.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.android.databinding.LayoutPedidoBinding;
import br.ufpr.ees.controlepedidos.android.viewmodel.PedidoViewModel;
import br.ufpr.ees.controlepedidos.domain.Pedido;

public class PedidoAdapter extends BaseAdapter {
    private final List<Pedido> pedidos;
    private final Activity activity;

    public PedidoAdapter(List<Pedido> pedidos, Activity activity) {
        this.pedidos = pedidos;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return pedidos.size();
    }

    @Override
    public Object getItem(int position) {
        return pedidos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return pedidos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutPedidoBinding layoutPedidoBinding;

        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.layout_pedido, null);
            layoutPedidoBinding = DataBindingUtil.bind(convertView);
            convertView.setTag(layoutPedidoBinding);
        } else {
            layoutPedidoBinding = (LayoutPedidoBinding) convertView.getTag();
        }
        PedidoViewModel pedidoViewModel = new PedidoViewModel();
        pedidoViewModel.setPedido(pedidos.get(position));
        layoutPedidoBinding.setPedido(pedidoViewModel);
        return layoutPedidoBinding.getRoot();

    }
}
