package br.ufpr.ees.controlepedidos.android.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.android.databinding.LayoutProdutoBinding;
import br.ufpr.ees.controlepedidos.domain.Produto;

public class ProdutoAdapter extends BaseAdapter {
    private final List<Produto> produtos;
    private final Activity activity;

    public ProdutoAdapter(List<Produto> produtos, Activity activity) {
        this.produtos = produtos;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return produtos.size();
    }

    @Override
    public Object getItem(int position) {
        return produtos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return produtos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutProdutoBinding produtoBinding;

        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.layout_produto, null);
            produtoBinding = DataBindingUtil.bind(convertView);
            convertView.setTag(produtoBinding);
        } else {
            produtoBinding = (LayoutProdutoBinding) convertView.getTag();
        }
        produtoBinding.setProduto(produtos.get(position));
        return produtoBinding.getRoot();
    }
}
