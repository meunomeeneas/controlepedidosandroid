package br.ufpr.ees.controlepedidos.android.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import br.ufpr.ees.controlepedidos.android.R;
import br.ufpr.ees.controlepedidos.android.databinding.LayoutClienteBinding;
import br.ufpr.ees.controlepedidos.domain.Cliente;

public class ClienteAdapter extends BaseAdapter {
    private final List<Cliente> clientes;
    private final Activity activity;

    public ClienteAdapter(List<Cliente> clientes, Activity activity) {
        this.clientes = clientes;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return clientes.size();
    }

    @Override
    public Object getItem(int position) {
        return clientes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return clientes.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutClienteBinding clienteBinding;

        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.layout_cliente, null);
            clienteBinding = DataBindingUtil.bind(convertView);
            convertView.setTag(clienteBinding);
        } else {
            clienteBinding = (LayoutClienteBinding) convertView.getTag();
        }
        clienteBinding.setCliente(clientes.get(position));
        return clienteBinding.getRoot();
    }

}
