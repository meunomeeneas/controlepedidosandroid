package br.ufpr.ees.controlepedidos.android.activity;

import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.concurrent.TimeUnit;

import br.ufpr.ees.controlepedidos.android.R;

public class PrincipalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(2));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {
            case R.id.clientes:
                startActivity(new Intent(this, ClientesActivity.class));
                return true;
            case R.id.produtos:
                startActivity(new Intent(this, ProdutosActivity.class));
                return true;
            case R.id.efetuar_pedido:
                startActivity(new Intent(this, PedidosActivity.class));
                return true;
            case R.id.pesquisar_pedidos:
                startActivity(new Intent(this, PesquisarPedidosActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(menu);
        }
    }

}
