package br.ufpr.ees.controlepedidos.android;

import android.app.Application;

import br.ufpr.ees.controlepedidos.android.service.ClienteService;
import br.ufpr.ees.controlepedidos.android.service.PedidoService;
import br.ufpr.ees.controlepedidos.android.service.ProdutoService;
import br.ufpr.ees.controlepedidos.android.service.Retrofit2Client;

public class ControlePedidosApp extends Application {
    static ClienteService clienteService;
    static ProdutoService produtoService;
    static PedidoService pedidoService;

    @Override
    public void onCreate() {
        super.onCreate();
        clienteService = new Retrofit2Client().create().create(ClienteService.class);
        produtoService = new Retrofit2Client().create().create(ProdutoService.class);
        pedidoService = new Retrofit2Client().create().create(PedidoService.class);
    }

    public static ClienteService getClienteService() { return clienteService; }

    public static ProdutoService getProdutoService() {
        return produtoService;
    }

    public static PedidoService getPedidoService() {
        return pedidoService;
    }
}
